// Fill out your copyright notice in the Description page of Project Settings.

#include "Zadatak1.h"
#include "MyActorComponent.h"

// Sets default values for this component's properties
UMyActorComponent::UMyActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UMyActorComponent::BeginPlay()
{
	Super::BeginPlay();
	Kinect.Init();
	//UE_LOG(KinectLog, Warning, TEXT("Kinect sensor is on!\n"));
	// ...

}

//Called when the game ends
void UMyActorComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//	EEndPlayReason::Type::Quit;
	Kinect.Shutdown();
	//UE_LOG(KinectLog, Warning, TEXT("Shut down the Kinect\n"));
	// ...

}

// Called every frame
void UMyActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	TArray<FVector> vectorArray;
	FVector leftHandPosition;
	//float angle = 0;
	FVector angle = FVector(0, 0, 0);

	if (Kinect.InitFlag == 1) 
	{
		//UE_LOG(KinectLog, Warning, TEXT("Kinect sensor is on, starting update...\n"));
		vectorArray = Kinect.Update(DeltaTime);
		
		switch (Kinect.leftHandState) {
		case HandState::HandState_Closed: leftHandState = HandStates::Closed; break;
		case HandState::HandState_Open : leftHandState = HandStates::Opened; break;
		case HandState::HandState_Lasso: leftHandState = HandStates::Lasso; break;
		default: leftHandState = HandStates::UnKnown; break;
		}

		switch (Kinect.rightHandState) {
		case HandState::HandState_Closed: rightHandState = HandStates::Closed; break;
		case HandState::HandState_Open: rightHandState = HandStates::Opened; break;
		case HandState::HandState_Lasso: rightHandState = HandStates::Lasso; break;
		default: rightHandState = HandStates::UnKnown; break;
		}

		leftHandPosition = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);

		//if left and right hand is in state lasso freeze the scene to manipulate the point cloud
		if (isUpdating == 0)
		{
			Kinect.skip = -1;
		}

		if (isUpdating)
		{
			Kinect.skip = 10;
		}

		//if left hand is closed enter translation
		if (translationFlag != 1 && Kinect.leftHandState == HandState_Closed && Kinect.rightHandState != HandState_Closed)
		{
			translate = 1;
			translationFlag = 1;
			//sceneFreezed = 1;
			//startRightHandPosition = FVector(-Kinect.rightHandPos.X, -Kinect.rightHandPos.Z, Kinect.rightHandPos.Y);
			startLeftHandPosition = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);
		}
		//take positions for translation 
		else if (translationFlag == 1 && Kinect.leftHandState == HandState_Closed && Kinect.rightHandState != HandState_Closed)
		{
			currentLeftHandPosition = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);
			translationValue = startLeftHandPosition.operator+(-currentLeftHandPosition);
			translationValue = FVector(-translationValue.X, -translationValue.Y, -translationValue.Z);
			//sceneFreezed = 1;
		}
		//stop translation
		else
		{
			translationFlag = 0;
			translate = 0;
			translationValue = FVector(0, 0, 0);
			//sceneFreezed = 0;
		}

		//FVector leftHand = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Y, Kinect.leftHandPos.Z);
		//FVector rightHand = FVector(-Kinect.rightHandPos.X, -Kinect.rightHandPos.Y, Kinect.rightHandPos.Z);
		//angle = angleBetween(leftHand, zeroPosition);

		//leftHandPosition = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);


	//	if (mode == 0 && sceneFreezed == 1)
		//{
			
			if (rotationFlag != 1 && Kinect.leftHandState == HandState_Lasso && Kinect.rightHandState != HandState_Lasso)
			{
				rotate = 1;
				rotationFlag = 1;
				//angle = FRotator(0, 0, 0);
				//sceneFreezed = 1;
				//startRightHandPosition = FVector(-Kinect.rightHandPos.X, -Kinect.rightHandPos.Z, Kinect.rightHandPos.Y);
				startLeftHandPositionForRotation = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);
			}
			else if (rotationFlag == 1 && Kinect.leftHandState == HandState_Lasso && Kinect.rightHandState != HandState_Lasso)
			{
				currentLeftHandPositionForRotation = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);
				//rotationValue = startLeftHandPosition.operator+(-currentRightHandPosition);
				//rotationValue = FVector(-rotationValue.X, -rotationValue.Y, -rotationValue.Z);
				//sceneFreezed = 1;
				FVector leftHand = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Y, Kinect.leftHandPos.Z);
				//FVector rightHand = FVector(-Kinect.rightHandPos.X, -Kinect.rightHandPos.Y, Kinect.rightHandPos.Z);
				//FVector zeroPosition = FVector(0, 0, 0);
				//angle = angleBetween(currentRightHandPosition, startRightHandPosition);
				angle = startLeftHandPositionForRotation.operator+(-currentLeftHandPositionForRotation);
				angle = FVector(-angle.X, -angle.Y, -angle.Z);
			}
			else
			{
				rotationFlag = 0;
				rotate = 0;
				//rotationValue = FVector(0, 0, 0);
				angle = FVector(0, 0, 0);
				//sceneFreezed = 0;
			}

			//if right hand is in lasso state then we unfreeze the scene to get new point cloud data
			/*else if (sceneFreezed == 1 && Kinect.leftHandState == HandState_Lasso && Kinect.rightHandState == HandState_Lasso)
			{
			Kinect.skip = 10;
			sceneFreezed = 0;
			}*/

			//if the scene is freezed and left hand is closed than we rotate the pointcloud
			/*if (sceneFreezed == 1 && Kinect.rightHandState == HandState_Closed)
			{
				rotate = 1;
				//FVector leftHand = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Y, Kinect.leftHandPos.Z);
				
			}
			else
			{
				rotate = 0;
			}*/


			//if left hand is closed enter translation
			if (scaleFlag != 1 && Kinect.rightHandState == HandState_Closed)
			{
				//scale = 1;
				scaleFlag = 1;
				//sceneFreezed = 1;
				//startRightHandPosition = FVector(-Kinect.rightHandPos.X, -Kinect.rightHandPos.Z, Kinect.rightHandPos.Y);
				startRightHandPosition = FVector(-Kinect.rightHandPos.X, -Kinect.rightHandPos.Z, Kinect.rightHandPos.Y);
			}
			//take positions for translation 
			else if (scaleFlag == 1 && Kinect.rightHandState == HandState_Closed)
			{
				currentRightHandPosition = FVector(-Kinect.rightHandPos.X, -Kinect.rightHandPos.Z, Kinect.rightHandPos.Y);
				scaleValue = startRightHandPosition.operator+(-currentRightHandPosition);
				scaleValue = FVector(-scaleValue.X, -scaleValue.Y, -scaleValue.Z);
				//sceneFreezed = 1;
			}
			//stop translation
			else
			{
				scaleFlag = 0;
				//scale = 0;
				scaleValue = FVector(0, 0, 0);
				//sceneFreezed = 0;
			}


		//}
		/*else if (mode == 1 && sceneFreezed == 1)
		{
			//UE_LOG(KinectLog, Warning, TEXT("editor mode!\n"));

			//ako ne zelimo selectati moramo otvoriti desnu ruku
			if (Kinect.rightHandState == HandState_Open) {
				select = 0;
			}
			//ako zelimo uci u select moramo zatvoriti desnu ruku
			if (Kinect.rightHandState == HandState_Closed) {
				select = 1;
			}

			if (paste == 1) { //ova provjera za paste mozda ne treba nego mjenjamo paste u BP
				if (Kinect.leftHandState == HandState_Lasso) {
					//ako zelimo pastati nakon cut-a innerCut == 1 i selectane kugle postavljamo na lokaciju lijeve ruke
					//pasteAttached = 1;
					pasted = 1;
					//currentLeftHandPosition = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);
				}
				if (Kinect.leftHandState == HandState_Open) {
					//ako zelimo pastati nakon cut-a innerCut == 1 i selectane kugle postavljamo na lokaciju lijeve ruke
					//pasteAttached = 1;
					pasted = 0;
					//currentLeftHandPosition = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);
				}
			}
		//ako je lijeva ruka zatvorena izvrsava se traslacija a dok je otvorena ne desava se nista i moze sluziti za odabir na izborniku
		
		/*	if (paste == 1) {
				if (Kinect.leftHandState == HandState_Open) { //paste something at left hand location
					innerPaste = 1;
					//currentLeftHandPosition = FVector(-Kinect.leftHandPos.X, -Kinect.leftHandPos.Z, Kinect.leftHandPos.Y);
				}
				if (Kinect.leftHandState == HandState_Closed) { //do not paste something at left hand location
					innerPaste = 0;
				}
			}*/
	

		//}

		/*if (Kinect.handRaisedUp == 1)
		{
			UE_LOG(KinectLog, Warning, TEXT("Mile mode!\n"));
		}*/


		GetVectors(vectorArray, translationValue, angle, scaleValue);
	}

}

/*
// Loads Point Cloud from file
TArray<FVector> UMyActorComponent::LoadPointCloud(const FString& FileName)
{
	FString FileDataString;
	TArray<uint8> FileDataArray;

	int32 HeaderEndByte;
	//"D:\\dev\\Unreal Projects\\Zadatak1\\ProjectFiles\\vase.ply"
	FFileHelper::LoadFileToString(FileDataString, *FileName);

	HeaderEndByte = FileDataString.Find("end_header") + 11;
	UE_LOG(KinectLog, Warning, TEXT("Header end %i"), HeaderEndByte);

	FFileHelper::LoadFileToArray(FileDataArray, *FileName);
	//float coordinateX, coordinateY, coordinateZ;
	PCPoint point;

	//uint8 colorR, colorG, colorB, colorA;

	TArray<FVector> vectorArray;
	int32 vertexCount = 0;
	uint8 * StartDataPosition = FileDataArray.GetData() + HeaderEndByte;

	//for (int32 FilePosition = HeaderEndByte; FilePosition < FileDataArray.Num(); )
	for (uint8 * DataPosition = FileDataArray.GetData() + HeaderEndByte; DataPosition < StartDataPosition + FileDataArray.Num() && vertexCount < 2000; )
	{
		//GetPointFromPointer(DataPosition, &point);
		memcpy(&point, DataPosition, sizeof(point));
		// Ako preska�emo color podatke, onda se pozicija pove�ava za 16 (12 za u�itane koordinate i 4 za boje)
		// Ako ne preska�em boje, onda se treba pove�ati za 12
		//DataPosition += 16;
		DataPosition += 12;


		vectorArray.Add(FVector(point.X * 10, point.Y * 10, point.Z * 10));
		vertexCount++;


	}

	return vectorArray;

}*/

//returns vector from 3 given floats
FVector UMyActorComponent::FloatsToVector(float fX, float fY, float fZ)
{
	return FVector(fX, fY, fZ);
}

//make array of vectors from array of integers
TArray<FVector> UMyActorComponent::ArrayToVectors(TArray<int32> IntArray)
{
	TArray<FVector> VectArray;

	for (int32 i = 0; i < IntArray.Num(); i++)
	{
		VectArray.Add(FVector(i, IntArray[i], 0));
	}

	return VectArray;
}

TArray<FVector> UMyActorComponent::Array2DToVectors(TArray<int32> IntArray)
{
	TArray<int32> IntArray2D[10][10];
	TArray<FVector> VectArray;

	int32 k = 0, n = 0;
	for (int32 i = 0; i < 10; i++)
	{
		for (int32 j = 0; j < 10; j++)
		{
			n = (i + j) * 100;
			IntArray2D[i][j].Add(IntArray[k]);
			VectArray.Add(FVector(n, IntArray[k], j));
			k++;
		}
	}
	return VectArray;
}

void KinectStarter::Init()
{
	HRESULT hr;

	//get the kinect sensor
	hr = GetDefaultKinectSensor(&m_sensor);
	if (FAILED(hr))
	{
		//UE_LOG(KinectLog, Warning, TEXT("Failed to find the kinect sensor!\n"));
		InitFlag = 0;
		exit(10);

	}
	m_sensor->Open();

	//get the depth frame source
	IDepthFrameSource* depthFrameSource;
	hr = m_sensor->get_DepthFrameSource(&depthFrameSource);
	if (FAILED(hr))
	{
		//UE_LOG(KinectLog, Warning, TEXT("Failed to get the depth frame source.\n"));
		InitFlag = 0;
		exit(10);
	}

	//get depth frame description
	IFrameDescription *frameDesc;
	hr = depthFrameSource->get_FrameDescription(&frameDesc);
	if (FAILED(hr)) 
	{
		InitFlag = 0;
		UE_LOG(KinectLog, Warning, TEXT("Failed to get the depth frame description.\n"));
	}
	frameDesc->get_Width(&m_depthWidth);
	frameDesc->get_Height(&m_depthHeight);

	//get the depth frame reader
	hr = depthFrameSource->OpenReader(&m_depthFrameReader);
	if (FAILED(hr))
	{
		//UE_LOG(KinectLog, Warning, TEXT("Failed to open the depth frame reader!\n"));
		InitFlag = 0;
		exit(10);
	}
	//release depth frame source
	SafeRelease(depthFrameSource);

	//allocate depth buffer
	m_depthBuffer = new uint16[m_depthWidth * m_depthHeight];

	//gesture stuff

	// get body source
	hr = m_sensor->get_BodyFrameSource(&pBodySource);
	if (FAILED(hr)) 
	{
		//UE_LOG(KinectLog, Warning, TEXT("Error : IKinectSensor::get_BodyFrameSource()\n"));
		exit(10);
	}

	// get body reader
	hr = pBodySource->OpenReader(&pBodyReader);
	if (FAILED(hr)) 
	{
		//(KinectLog, Warning, TEXT("Error : IBodyFrameSource::OpenReader()\n"));
		exit(10);
	}

	/*
	for (int count = 0; count < BODY_COUNT; count++) 
	{
		// Source
		hr = CreateVisualGestureBuilderFrameSource(m_sensor, 0, &pGestureSource[count]);
		if (FAILED(hr)) 
		{
			//UE_LOG(KinectLog, Warning, TEXT("Error : CreateVisualGestureBuilderFrameSource()\n"));

			printf("Error : CreateVisualGestureBuilderFrameSource()");
			exit(10);
		}

		// Reader
		hr = pGestureSource[count]->OpenReader(&pGestureReader[count]);
		if (FAILED(hr)) 
		{
			//UE_LOG(KinectLog, Warning, TEXT("Error : IVisualGestureBuilderFrameSource::OpenReader()\n"));
			exit(10);
		}
	}
	*/
	
	/*
	hr = CreateVisualGestureBuilderDatabaseInstanceFromFile(L"C:\\Users\\Matija\\Documents\\Unreal Projects\\PointCloudEditorViewer\\Zadatak1\\ProjectFiles\\Gestures\\HandClap.gba"/*L"Swipe.gba"*//*, &pGestureDatabase); 
	if (FAILED(hr)) 
	{
		//UE_LOG(KinectLog, Warning, TEXT("Error : CreateVisualGestureBuilderDatabaseInstanceFromFile()\n"));
		exit(10);
	}
	/*
	// Create Gesture Dataase from File (*.gba)
	//hr = CreateVisualGestureBuilderDatabaseInstanceFromFile(L"C:\\Users\\Matija\\Desktop\\Zadatak1\\ProjectFiles\\Gestures\\Swipe.gba"/*L"Swipe.gba", &pGestureDatabase);*/
	/*if (FAILED(hr)) {
		UE_LOG(KinectLog, Warning, TEXT("Error : CreateVisualGestureBuilderDatabaseInstanceFromFile()\n"));
		exit(10);
	}

	// Add Gesture
	uint32 gestureCount = 0;
	hr = pGestureDatabase->get_AvailableGesturesCount(&gestureCount);
	if (FAILED(hr) || !gestureCount)
	{
		//UE_LOG(KinectLog, Warning, TEXT("Error : IVisualGestureBuilderDatabase::get_AvailableGesturesCount()\n"));
		exit(10);
	}

	hr = pGestureDatabase->get_AvailableGestures(gestureCount, &pGesture);
	if (SUCCEEDED(hr) && pGesture != nullptr)
	{
		for (int count = 0; count < BODY_COUNT; count++) 
		{
			hr = pGestureSource[count]->AddGesture(pGesture);
			if (FAILED(hr)) 
			{
				//UE_LOG(KinectLog, Warning, TEXT("Error : IVisualGestureBuilderFrameSource::AddGesture()\n"));
				exit(10);
			}

			hr = pGestureSource[count]->SetIsEnabled(pGesture, true);
			if (FAILED(hr))
			{
				//UE_LOG(KinectLog, Warning, TEXT("Error : IVisualGestureBuilderFrameSource::SetIsEnabled()\n"));
				exit(10);
			}
		}
	}
	*/
	//if everything went ok, set flag to 1 to prevent going into init again
	InitFlag = 1;

}

TArray<FVector> KinectStarter::Update(float deltaTime)
{
	HRESULT hr;
	FVector vector;
	TArray<FVector> vectorArray;
	
	//depth stuff
	IDepthFrame* depthFrame;
	hr = m_depthFrameReader->AcquireLatestFrame(&depthFrame);
	if (FAILED(hr)) 
	{
		//UE_LOG(KinectLog, Warning, TEXT("Faild to get depth frame (update)\n"));
		return vectorArray;
	}

	//UE_LOG(KinectLog, Warning, TEXT("Copying data!\n"));
	hr = depthFrame->CopyFrameDataToArray(
		m_depthWidth * m_depthHeight, m_depthBuffer);

	if (FAILED(hr))
	{
		SafeRelease(depthFrame);
		//UE_LOG(KinectLog, Warning, TEXT("Oh no, something went wrong while copying!\n"));
		return vectorArray;
	}
	SafeRelease(depthFrame);

	//copy depth data to vector array

	//gesture stuff
	// Frame
	IBodyFrame* pBodyFrame = nullptr;
	hr = pBodyReader->AcquireLatestFrame(&pBodyFrame);
	if (SUCCEEDED(hr)) 
	{
		IBody* pBody[BODY_COUNT] = { 0 };
		hr = pBodyFrame->GetAndRefreshBodyData(BODY_COUNT, pBody);
		if (SUCCEEDED(hr))
		{
			for (int count = 0; count < BODY_COUNT; count++) 
			{
				BOOLEAN bTracked = false;
				hr = pBody[count]->get_IsTracked(&bTracked);
				if (SUCCEEDED(hr) && bTracked) 
				{
					// Joint
					Joint joint[JointType::JointType_Count];
					hr = pBody[count]->GetJoints(JointType::JointType_Count, joint);

					leftHandPos = joint[JointType_HandLeft].Position;
					rightHandPos = joint[JointType_HandRight].Position;
					
					//handstate left
					hr = pBody[count]->get_HandLeftState(&leftHandState);
					if (SUCCEEDED(hr))
					{
						if (leftHandState == HandState_Closed) 
						{
							//UE_LOG(KinectLog, Warning, TEXT("LEFT CLOSED HAND!\n"));
						}
					}
					//handstate right
					hr = pBody[count]->get_HandRightState(&rightHandState);
					if (SUCCEEDED(hr))
					{
						if (rightHandState == HandState_Closed)
						{
							//UE_LOG(KinectLog, Warning, TEXT("RIGHT CLOSED HAND!\n"));
						}
					}

					// Set TrackingID to Detect Gesture
				/*	UINT64 trackingId = _UI64_MAX;
					hr = pBody[count]->get_TrackingId(&trackingId);
					if (SUCCEEDED(hr))
					{
						pGestureSource[count]->put_TrackingId(trackingId);
					}*/
				}
			}
		}
		for (int count = 0; count < BODY_COUNT; count++) 
		{
			SafeRelease(pBody[count]);
		}
	}
	SafeRelease(pBodyFrame);

	if (skip <= 0) return vectorArray;

	float X, Y, Z;
	CameraParams cameraParameters;

	//UE_LOG(KinectLog, Warning, TEXT("Before for loop.\n"));
	for (int i = 0; i < m_depthWidth; i += skip)
	{
		for (int j = 0; j < m_depthHeight; j += skip)
		{
			X = (float)i;
			Y = (float)j;
			Z = float(m_depthBuffer[i + j * m_depthWidth]); //offset

			//UE_LOG(LogClass, Log, TEXT("X: %f"), X);
			//UE_LOG(LogClass, Log, TEXT("y: %f"), Y);
			//UE_LOG(LogClass, Log, TEXT("z: %f"), Z);

			//UE_LOG(LogClass, Log, TEXT("Sirina: %d"), m_depthWidth);
			//UE_LOG(LogClass, Log, TEXT("Visina: %d"), m_depthHeight);

			/*
			if (Z < 800 || Z > 8000)
			{
				continue;
			}
			*/

			if (Z < 800 || Z > 4500)
			{
				vector = FVector(0, 0, 0);
			}
			else
			{
				vector = FVector((X - cameraParameters.cx) * Z / cameraParameters.fx / -1, Z, (Y - cameraParameters.cy) * Z / cameraParameters.fy / -1);
			}


			//vector = UMyActorComponent::PosToCameraPos(vector);
			
			//vector = FVector(X * 7., Y * 7., Z / 10);

			//UE_LOG(LogClass, Log, TEXT("Poslije kamera"));
			//UE_LOG(LogClass, Log, TEXT("X: %f"), vector.X);
			//UE_LOG(LogClass, Log, TEXT("y: %f"), vector.Y);
			//UE_LOG(LogClass, Log, TEXT("z: %f"), vector.Z);

			//vector = FVector(vector.X / -10, vector.Z / 10, vector.Y / -10);
			vectorArray.Add(vector);

			//UE_LOG(LogClass, Log, TEXT("Poslije"));
			//UE_LOG(LogClass, Log, TEXT("X: %f"), vector.X);
			//UE_LOG(LogClass, Log, TEXT("y: %f"), vector.Y);
			//UE_LOG(LogClass, Log, TEXT("z: %f"), vector.Z);

			// Detect Gesture
			/*for (int count = 0; count < BODY_COUNT; count++) 
			{
				IVisualGestureBuilderFrame* pGestureFrame = nullptr;
				hr = pGestureReader[count]->CalculateAndAcquireLatestFrame(&pGestureFrame);
				if (SUCCEEDED(hr) && pGestureFrame != nullptr) 
				{
					BOOLEAN bGestureTracked = false;
					hr = pGestureFrame->get_IsTrackingIdValid(&bGestureTracked);
					if (SUCCEEDED(hr) && bGestureTracked) 
					{
						// Discrete Gesture (Sample HandUp.gba is Action to Hand Up above the head.)
						IDiscreteGestureResult* pGestureResult = nullptr;
						hr = pGestureFrame->get_DiscreteGestureResult(pGesture, &pGestureResult);
						if (SUCCEEDED(hr) && pGestureResult != nullptr)
						{
							BOOLEAN bDetected = false;
							hr = pGestureResult->get_Detected(&bDetected);
							if (SUCCEEDED(hr) && bDetected)
							{
								//UE_LOG(KinectLog, Warning, TEXT("Detected Gesture.\n"));
								if (handRaisedUp == 1)
									handRaisedUp = 0;
								else if (handRaisedUp == 0)
									handRaisedUp = 1;
							}
							else
							{
								UE_LOG(KinectLog, Warning, TEXT("Not detected Gesture.\n"));
								handRaisedUp = 0;
							}
						}
						SafeRelease(pGestureResult);
					}
				}
				SafeRelease(pGestureFrame);
			}*/
		}
	}

	//UE_LOG(KinectLog, Warning, TEXT("For loop ended.\n"));

	return vectorArray;
}

//shutdown kinect, clear data
void KinectStarter::Shutdown()
{
	//UE_LOG(KinectLog, Warning, TEXT("Shuting Kinect down.\n"));

	InitFlag = 0;
	//if(m_depthBuffer) delete[] m_depthBuffer;
	SafeRelease(m_depthFrameReader);

	//gesture stuff
	SafeRelease(pBodySource);
	SafeRelease(pBodyReader);

	/*for (int count = 0; count < BODY_COUNT; count++) 
	{
		SafeRelease(pGestureSource[count]);
		SafeRelease(pGestureReader[count]);
	}

	SafeRelease(pGestureDatabase);
	SafeRelease(pGesture);*/

	if (m_sensor)
	{
		m_sensor->Close();
	}
	SafeRelease(m_sensor);
}

FVector UMyActorComponent::PosToCameraPos(FVector vector)
{
	CameraParams cameraParametars;
	FVector newVector;

/*
	if (vector.Z < 800 || vector.Z > 8000)
	{
		newVector.Z = 0;
		newVector.X = 0;
		newVector.Y = 0;
	}
	else
	{
		newVector.Z = vector.Z;
		newVector.X = (vector.X - cameraParametars.cx) * vector.Z / cameraParametars.fx;
		newVector.Y = (vector.Y - cameraParametars.cy) * vector.Z / cameraParametars.fy;
	}
*/
	if (vector.Z < 800 || vector.Z > 8000)
	{
		vector.Z = 8000;
	}

	newVector.Z = vector.Z;
	newVector.X = (vector.X - cameraParametars.cx) * vector.Z / cameraParametars.fx;
	newVector.Y = (vector.Y - cameraParametars.cy) * vector.Z / cameraParametars.fy;

	return newVector;
}

//http://www.cplusplus.com/forum/general/95842/
float KinectStarter::getAngle(float x1, float y1, float z1, float x2, float y2, float z2)
{
	float theta = atan2(z1 - z2, x1 - x2);
	return -theta * 180 / 3.1415926;
}

float KinectStarter::getAngleToKCY(float x1, float y1, float z1, float x2, float y2, float z2)
{
	float dist = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2) + pow(z1 - z2, 2));
	float dist2 = sqrt(pow(x1 - x2, 2) + pow(z1 - z2, 2));
	return acos(dist2 / dist) * 180 / 3.1415926;
}

float KinectStarter::getAngleToKCX(float x1, float y1, float z1, float x2, float y2, float z2)
{
	float dist = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2) + pow(z1 - z2, 2));
	float dist2 = sqrt(pow(y1 - y2, 2) + pow(z1 - z2, 2));
	return asin(dist2 / dist) * 180 / 3.1415926;
}

//https://rosettacode.org/wiki/Vector_products#C.2B.2B
FVector crossProduct(FVector v1, FVector v2) {
	float a = v1.Y * v2.Z - v1.Z * v2.Y;
	float b = v1.Z * v2.X - v1.X * v2.Z;
	float c = v1.X * v2.Y - v1.Y * v2.X;
	//FVector(a, b, c);
	return FVector(a, b, c);
}



/** note v1 and v2 dont have to be nomalised, thanks to minorlogic for telling me about this:
* http://www.euclideanspace.com/maths/algebra/vectors/angleBetween/minorlogic.htm
*/

FRotator UMyActorComponent::angleBetween(FVector v1, FVector v2) {

	float dotProduct = v1.X*v2.X + v1.Y*v2.Y + v1.Z*v2.Z;

	FVector axis = crossProduct(v1, v2);

	float qw = (float)sqrt(v1.Size()*v2.Size()) + dotProduct;

	if (qw < 0.0001)
	{ // vectors are 180 degrees apart
		FQuat returnFQuat = FQuat(0, -v1.Z, v1.Y, v1.X);
		returnFQuat.Normalize();
		FRotator returnRotator = returnFQuat.Rotator();
		return returnRotator;
	}

	FQuat returnFQuat = FQuat(qw, axis.X, axis.Y, axis.Z);
	returnFQuat.Normalize();
	FRotator returnRotator = returnFQuat.Rotator();
	return returnRotator;
}

FVector UMyActorComponent::translatePoint(FVector vectorToTranslate, FVector translationValue)
{
	FTranslationMatrix translationMatrix = FTranslationMatrix(translationValue);
	FVector newVector = translationMatrix.TransformPosition(vectorToTranslate);
	return newVector;
}

FVector UMyActorComponent::rotatePointUEZ(FVector translationValue, FVector vectorToRotate, float angle)
{
	FTranslationMatrix translationMatrix = FTranslationMatrix(-translationValue);
	FVector newVector = translationMatrix.TransformPosition(vectorToRotate);
	float x = 0, y = 0, z = 0;
	//float angle = 0.1;
	
	FMatrix rotationMatrix = FMatrix();

	rotationMatrix.M[0][0] = cos(angle);
	rotationMatrix.M[0][1] = sin(angle);
	rotationMatrix.M[1][0] = -sin(angle);
	rotationMatrix.M[1][1] = cos(angle);

	rotationMatrix.M[2][2] = 1;
	rotationMatrix.M[3][3] = 1;

	rotationMatrix.M[0][2] = 0;
	rotationMatrix.M[0][3] = 0;
	rotationMatrix.M[1][2] = 0;
	rotationMatrix.M[1][3] = 0;
	rotationMatrix.M[2][0] = 0;
	rotationMatrix.M[2][1] = 0;
	rotationMatrix.M[2][3] = 0;
	rotationMatrix.M[3][0] = 0;
	rotationMatrix.M[3][1] = 0;
	rotationMatrix.M[3][2] = 0;

	x = (rotationMatrix.M[0][0] * newVector.X + rotationMatrix.M[0][1] * newVector.Y + rotationMatrix.M[0][2] * newVector.Z + rotationMatrix.M[0][3]);
	y = (rotationMatrix.M[1][0] * newVector.X + rotationMatrix.M[1][1] * newVector.Y + rotationMatrix.M[1][2] * newVector.Z + rotationMatrix.M[1][3]);
	z = (rotationMatrix.M[2][0] * newVector.X + rotationMatrix.M[2][1] * newVector.Y + rotationMatrix.M[2][2] * newVector.Z + rotationMatrix.M[2][3]);
	
	translationMatrix = FTranslationMatrix(translationValue);
	newVector = FVector(x, y, z);
	newVector = translationMatrix.TransformPosition(newVector);

	return newVector;
}

FVector UMyActorComponent::rotatePointUEY(FVector translationValue, FVector vectorToRotate, float angle)
{
	FTranslationMatrix translationMatrix = FTranslationMatrix(-translationValue);
	FVector newVector = translationMatrix.TransformPosition(vectorToRotate);
	float x = 0, y = 0, z = 0;
	//float angle = 0.1;

	FMatrix rotationMatrix = FMatrix();

	rotationMatrix.M[0][0] = cos(angle);
	rotationMatrix.M[0][2] = sin(angle);
	rotationMatrix.M[2][0] = -sin(angle);
	rotationMatrix.M[2][2] = cos(angle);

	rotationMatrix.M[1][1] = 1;
	rotationMatrix.M[3][3] = 1;

	rotationMatrix.M[0][1] = 0;
	rotationMatrix.M[1][0] = 0;
	rotationMatrix.M[0][3] = 0;
	rotationMatrix.M[1][2] = 0;
	rotationMatrix.M[1][3] = 0;
	rotationMatrix.M[2][1] = 0;
	rotationMatrix.M[2][3] = 0;
	rotationMatrix.M[3][0] = 0;
	rotationMatrix.M[3][1] = 0;
	rotationMatrix.M[3][2] = 0;

	x = (rotationMatrix.M[0][0] * newVector.X + rotationMatrix.M[0][1] * newVector.Y + rotationMatrix.M[0][2] * newVector.Z + rotationMatrix.M[0][3]);
	y = (rotationMatrix.M[1][0] * newVector.X + rotationMatrix.M[1][1] * newVector.Y + rotationMatrix.M[1][2] * newVector.Z + rotationMatrix.M[1][3]);
	z = (rotationMatrix.M[2][0] * newVector.X + rotationMatrix.M[2][1] * newVector.Y + rotationMatrix.M[2][2] * newVector.Z + rotationMatrix.M[2][3]);

	translationMatrix = FTranslationMatrix(translationValue);
	newVector = FVector(x, y, z);
	newVector = translationMatrix.TransformPosition(newVector);

	return newVector;
}

FVector UMyActorComponent::rotatePointUEX(FVector translationValue, FVector vectorToRotate, float angle)
{
	FTranslationMatrix translationMatrix = FTranslationMatrix(-translationValue);
	FVector newVector = translationMatrix.TransformPosition(vectorToRotate);
	float x = 0, y = 0, z = 0;
	//float angle = 0.1;

	FMatrix rotationMatrix = FMatrix();

	rotationMatrix.M[1][1] = cos(angle);
	rotationMatrix.M[1][2] = -sin(angle);
	rotationMatrix.M[2][1] = sin(angle);
	rotationMatrix.M[2][2] = cos(angle);

	rotationMatrix.M[0][0] = 1;
	rotationMatrix.M[3][3] = 1;

	rotationMatrix.M[0][2] = 0;
	rotationMatrix.M[2][0] = 0;
	rotationMatrix.M[0][1] = 0;
	rotationMatrix.M[1][0] = 0;
	rotationMatrix.M[0][3] = 0;
	rotationMatrix.M[1][3] = 0;
	rotationMatrix.M[2][3] = 0;
	rotationMatrix.M[3][0] = 0;
	rotationMatrix.M[3][1] = 0;
	rotationMatrix.M[3][2] = 0;

	x = (rotationMatrix.M[0][0] * newVector.X + rotationMatrix.M[0][1] * newVector.Y + rotationMatrix.M[0][2] * newVector.Z + rotationMatrix.M[0][3]);
	y = (rotationMatrix.M[1][0] * newVector.X + rotationMatrix.M[1][1] * newVector.Y + rotationMatrix.M[1][2] * newVector.Z + rotationMatrix.M[1][3]);
	z = (rotationMatrix.M[2][0] * newVector.X + rotationMatrix.M[2][1] * newVector.Y + rotationMatrix.M[2][2] * newVector.Z + rotationMatrix.M[2][3]);

	translationMatrix = FTranslationMatrix(translationValue);
	newVector = FVector(x, y, z);
	newVector = translationMatrix.TransformPosition(newVector);

	return newVector;
}

// Loads Point Cloud from file
TArray<FVector> UMyActorComponent::LoadPointCloud(const FString& FileName)
{
	FString FileDataString;
	TArray<FVector> vectorArray;
	int32 HeaderEndByte;
	float NewFloat = 0;
	FString Element = "";
	FVector VectorElement;
	int Counter = 0;

	FFileHelper::LoadFileToString(FileDataString, *FileName);
	HeaderEndByte = FileDataString.Find("end_header") + 11;

	for (int32 i = HeaderEndByte+1; i < FileDataString.Len(); i++) { 
																		 

		//UE_LOG(KinectLog, Warning, TEXT("index %d, element %c\n"), i, FileDataString[i]);

		Element += FileDataString[i];

		if (FileDataString[i] == ' ') {
			Counter++;
			//UE_LOG(KinectLog, Warning, TEXT("razmak\n"));
			NewFloat = FCString::Atof(*Element);

			if(Counter == 1) VectorElement.X = NewFloat;
			if(Counter == 2) VectorElement.Y = NewFloat;
			//if(Counter == 3) VectorElement.Z = NewFloat;

			Element = "";
			//UE_LOG(KinectLog, Warning, TEXT("new float %f\n"), NewFloat);
		}
		
		else if (FileDataString[i] == '\n') {
			Counter = 0;
			//UE_LOG(KinectLog, Warning, TEXT("novi red s razmakom\n"));
			NewFloat = FCString::Atof(*Element);
			VectorElement.Z = NewFloat;
			vectorArray.Add(FVector(VectorElement.X, VectorElement.Y, VectorElement.Z));
			Element = "";
			//UE_LOG(KinectLog, Warning, TEXT("new float %f\n"), NewFloat);
		}

	}

	return vectorArray;

}

// Save Point Cloud to file
void UMyActorComponent::SavePointCloud(TArray<FVector> VectorsToSave)
{
	FString FileDataString;
	FString SavePath = "C:\\Users\\Matija\\Documents\\Unreal Projects\\Zadatak1\\ProjectFiles\\SavedPC.ply";
	FString NewLine = "\n";
	int VertexNum = VectorsToSave.Num();
	FString VNum = FString::FromInt(VertexNum);

	//create header
	FileDataString = ("ply\n");
	FileDataString += ("format ascii 1.0\n");
	FileDataString += ("element vertex ");
	FileDataString += VNum;
	FileDataString += ("\nproperty float x\n");
	FileDataString += ("property float y\n");
	FileDataString += ("property float z\n");
	FileDataString += ("end_header\n");

	//convert floats to FString
	for (int i = 0; i < VectorsToSave.Num(); i++) {
		FString FloatToStringX = FString::SanitizeFloat(VectorsToSave[i].X);
		FileDataString += FloatToStringX;
		FileDataString += " ";
		FString FloatToStringY = FString::SanitizeFloat(VectorsToSave[i].Y);
		FileDataString += FloatToStringY;
		FileDataString += " ";
		FString FloatToStringZ = FString::SanitizeFloat(VectorsToSave[i].Z);
		FileDataString += FloatToStringZ;
		FileDataString += NewLine;
	}

	//save point cloud to file
	FFileHelper::SaveStringToFile(FileDataString, *SavePath);

}